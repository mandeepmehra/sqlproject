--liquibase formatted sql

--changeset Mandeep:2
create table employees_test (employee_id number primary key, commission_pct number, salary number);
